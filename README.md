[![Generic badge](https://img.shields.io/badge/code-bash-lightgrey.svg)](https://shields.io/)

[![Generic badge](https://img.shields.io/badge/Maintener-Alban_VIDAL-lightgrey.svg)](https://framagit.org/alban.vidal)
[![Generic badge](https://img.shields.io/badge/Maintener-Quentin_LEJARD-lightgrey.svg)](https://framagit.org/valde/)

![Generic badge](https://img.shields.io/badge/Ubuntu-18+-orange.svg) ![Generic badge](https://img.shields.io/badge/Debian-9+-blue.svg)

Deploy nextcloud with lxd
=========================

**Automated deploy your private nextcloud instance**

![Generic badge](https://img.shields.io/badge/Debian-9-blue.svg) => 
[![Generic badge](https://img.shields.io/badge/Nextcloud-15-blue.svg)](https://nextcloud.com/)

![Generic badge](https://img.shields.io/badge/Debian-10-blue.svg) => 
[![Generic badge](https://img.shields.io/badge/Nextcloud-16.0.1-blue.svg)](https://nextcloud.com/)

----------------------------------------

This scripts deploy this solution :
+ Private cloud: **Nextcloud**
+ Databases for Nextcloud: **MariaDB**
+ ssh gateway to backup Nextcloud data **openssh** (coming soon)
+ Online document editing: **Collabora Online** or **OnlyOffice** (coming soon)

The following features is enabled :
+ Auto generate SSL certificate with **Let's Encrypt**
+ Isolation betwen components with **lxd**

Needed:
+ LXD host already configured, see [install_conf_lxd](https://framagit.org/zorval/lxd_deploy_apps/install_conf_lxd) for more information

----------------------------------------

## Create configuration file for Netxcloud and start deploy

+ Copy `config/example_project_000-netxcloud-example-com` to `config/project_<YOUR_PROJECT_NAME>`
+ Edit values in `config/project_<YOUR_PROJECT_NAME>` file
+ Edit usernames and passwords in `config/project_<YOUR_PROJECT_NAME>` file
+ Start deploy by execute: `./deploy.sh <YOUR_PROJECT_NAME>`

