#!/bin/bash
# Delete anonymous users
mysql -e "DELETE FROM mysql.user WHERE User='';"
# Ensure the root user can not log in remotely
mysql -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
# Remove the test database
mysql -e "DROP DATABASE IF EXISTS test;
          DELETE FROM mysql.db WHERE Db='test' OR Db='test\_%';"

# Make our changes take effect
mysql -e "FLUSH PRIVILEGES"
