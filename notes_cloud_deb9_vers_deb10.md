Migration serveur Nextcloud Debian 9 vers Debian 10
===================================================

# Modification de sources

```bash
cat << EOF > /etc/apt/sources.list
deb http://deb.debian.org/debian/ buster main contrib non-free
deb http://deb.debian.org/debian/ buster-updates main contrib non-free
deb http://security.debian.org/debian-security buster/updates main
EOF

apt update
```

# Arrêt du serveur apache et php-fpm

```bash
systemctl stop apache2
systemctl stop php7.0-fpm.service
```

# Mise à niveau vers Debian 10

```bash
apt dist-upgrade
```

# Installation des paquets php7.3

```bash
apt install php7.3 php-fpm php-mysql php-bz2 php-cli php-curl php-gd php-gmp php-intl php-json php-mbstring php-readline php-xml php-zip
```

# Configuration FPM

```bash
sed -i 															\
	-e 's/^memory_limit =.*/memory_limit = 512M/' 				\
	-e 's/^upload_max_filesize =.*/upload_max_filesize = 100M/' \
	/etc/php/7.3/fpm/php.ini
```

# Redémarrage de FPM

```bash
systemctl restart php7.3-fpm.service
```

# Configuration Apache et FPM

```bash
a2disconf php7.0-fpm
a2enconf php7.3-fpm
```

# Redémarrage d'Apache

```bash
systemctl restart apache2
```

# Suppression des anciens paquets php7

```bash
apt purge php7.0 php7.0-bz2 php7.0-cli php7.0-common php7.0-curl php7.0-fpm php7.0-gd php7.0-gmp php7.0-intl php7.0-json php7.0-mbstring php7.0-mcrypt php7.0-mysql php7.0-opcache php7.0-readline php7.0-xml php7.0-zip
```

# Nettoyage des paquets obsolètes

```bash
apt autoremove
```

# À vérifier

## Vérifier les mise à jour auto

Dans le fichier `/etc/apt/apt.conf.d/50unattended-upgrades` 

```
Unattended-Upgrade::Mail "unattended@example.com";
Unattended-Upgrade::MailOnlyOnError "true";
```

# Mise à jour de Nextcloud en 16

À faire comme d'habitude
