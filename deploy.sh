#!/bin/bash

################################################################################
# BSD 3-Clause License
# 
# Copyright (c) 2018-2019, Alban Vidal <alban.vidal@zordhak.fr>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
# 
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
# 
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
################################################################################

################################################################################
##########                    Define color to output:                 ########## 
################################################################################
_WHITE_="tput sgr0"
_RED_="tput setaf 1"
_GREEN_="tput setaf 2"
_ORANGE_="tput setaf 3"
_BLUE_="tput setaf 4"
################################################################################

# Path of git repository
# ./
GIT_PATH="$(realpath ${0%/*})"

################################################################################

REPO_install_conf_lxd="https://framagit.org/zorval/lxd_deploy_apps/install_conf_lxd"

################################################################################

# Exit if LXD is not installed
if ! which lxd >/dev/null;then
    echo "$($_RED_)LXD is not installed$($_WHITE_)"
    echo "Please see $REPO_install_conf_lxd"
    exit 1
fi

# Exit if rvprx is not created
if [ $(lxc ls --format csv rvprx | grep RUNNING | wc -l) -eq 0 ] ; then
    echo "$($_RED_)RVPRX container don't exist or is not started$($_WHITE_)"
    echo "Please see $REPO_install_conf_lxd"
    exit 2
fi

################################################################################

if [ -f $GIT_PATH/config/00_VARS ]; then
    source $GIT_PATH/config/00_VARS
else
    echo
    echo "$($_BLUE_)File « $GIT_PATH/config/00_VARS » don't exist$($_WHITE_)"
    echo "(if you need more help, see $REPO_install_conf_lxd)"
    echo
    echo -n "$($_BLUE_)Please enter the path of the « install_conf_lxd » git directory: $($_WHITE_)"
    read GIT_PATH_install_conf_lxd
    echo

    # Test if "GIT_PATH_install_conf_lxd" is correctly set
    if [ ! -d "${GIT_PATH_install_conf_lxd}/config/" ] ; then
        echo "$($_RED_)path of the « install_conf_lxd » git directory is not correct$($_WHITE_)"
        echo "$($_RED_)EXIT$($_WHITE_)"
        exit 99
    fi

    # Wtite to config/00_VARS file
    echo "GIT_PATH_install_conf_lxd='$GIT_PATH_install_conf_lxd'" >> $GIT_PATH/config/00_VARS
fi

################################################################################
#### Source local conf

source $GIT_PATH/config/NEXTCLOUD_GLOBAL_VARS

################################################################################
#### Source « install_conf_lxd » conf

# Load Vars
source $GIT_PATH_install_conf_lxd/config/00_VARS

# Load Network Vars
source $GIT_PATH_install_conf_lxd/config/01_NETWORK_VARS

# Load Resources Vars
source $GIT_PATH_install_conf_lxd/config/02_RESOURCES_VARS

# Load Other vars 
# - LXD_DEPORTED_DIR
# - DEBIAN_RELEASE
# - LXD_DEFAULT_STORAGE_TYPE
source $GIT_PATH_install_conf_lxd/config/03_OTHER_VARS

################################################################################
################################################################################

## PROJECT NAME
if [ -z $1 ] ; then
    echo -n "$($_BLUE_)Please enter a project name: $($_WHITE_)"
    read PROJECT_NAME
    echo
else
    PROJECT_NAME=$1
    echo "$($_ORANGE_)Project name are $PROJECT_NAME$($_WHITE_)"
fi

# TODO
# Test project name « ./$\_ »
# Test if project name contains bad characters
# list : « » «.» «_» «/» «\»
if echo "$PROJECT_NAME"|grep -o '\ \._/\\' ; then
    echo "$($_RED_)Project name are not allowed$($_WHITE_)"
    echo "$($_RED_)EXIT$($_WHITE_)"
    exit 5
fi

# Test if config file are present
if [ -f $GIT_PATH/config/project_${PROJECT_NAME} ] ; then
    # Load project vars
    source $GIT_PATH/config/project_${PROJECT_NAME}
else
    echo
    echo "$($_RED_)ERROR$($_WHITE_)"
    echo "$($_ORANGE_)config file for this project is absent$($_WHITE_)"
    echo "$($_ORANGE_)Please create '$GIT_PATH/config/project_${PROJECT_NAME}' file$($_WHITE_)"
    echo
    echo "$($_RED_)EXIT$($_WHITE_)"
    exit 3
fi

# Define start name of containers
CT_NAME_START="nc-${PROJECT_NAME}"

################################################################################

# Test if directory already exist
if [ -d $LXD_DEPORTED_DIR/$CT_NAME_START ] ; then
    echo
    echo -e "$($_RED_)\t\t!! WARNING !!$($_WHITE_)"
    echo
    echo "$($_ORANGE_)Directory '$LXD_DEPORTED_DIR/$CT_NAME_START' already exist$($_WHITE_)"
    echo
    echo "execute 'ls -l $LXD_DEPORTED_DIR/$CT_NAME_START':"
    ls -l $LXD_DEPORTED_DIR/$CT_NAME_START
    echo
    echo "$($_ORANGE_)- Press 'CTRL +C' to abort$($_WHITE_)"
    echo "$($_ORANGE_)- Press 'ENTER' to continue$($_WHITE_)"
    read
fi

################################################################################

lxc launch $LXD_IMAGE_VERSION z-${CT_NAME_START}-template --profile default --profile privNet

echo "$($_ORANGE_)Wait dhcp...$($_WHITE_)"
sleep 5

################################################################################
#
# Configure template container

echo "$($_ORANGE_)Container TEMPLATE: Update, upgrade and install common packages$($_WHITE_)"

PACKAGES="git vim apt-utils bsd-mailx postfix"

if [ "$DEBIAN_RELEASE" == "stretch" ] ; then
    lxc exec z-${CT_NAME_START}-template -- bash -c "echo 'deb http://ftp.fr.debian.org/debian stretch-backports main' > /etc/apt/sources.list.d/stretch-backports.list"
fi

lxc exec z-${CT_NAME_START}-template -- bash -c "
    apt-get update > /dev/null
    DEBIAN_FRONTEND=noninteractive apt-get -y install $PACKAGES > /dev/null
    DEBIAN_FRONTEND=noninteractive apt-get -y upgrade > /dev/null
    # Basic Debian configuration
    mkdir -p /srv/git
    git clone https://framagit.org/zorval/config_system/basic_config_debian.git /srv/git/basic_config_debian
    # Setup config file for auto configuration
    >                                                /srv/git/basic_config_debian/conf
    echo 'UNATTENDED_EMAIL=\"$CLOUD_TECH_ADMIN_EMAIL\"' >> /srv/git/basic_config_debian/conf
    echo 'GIT_USERNAME=\"$HOSTNAME\"'             >> /srv/git/basic_config_debian/conf
    echo 'GIT_EMAIL=\"root@$HOSTNAME\"'           >> /srv/git/basic_config_debian/conf
    echo 'SSH_EMAIL_ALERT=\"$CLOUD_TECH_ADMIN_EMAIL\"'  >> /srv/git/basic_config_debian/conf
    # Launch auto configuration script
    /srv/git/basic_config_debian/auto_config.sh
"

# Postfix default conf file
echo "$($_ORANGE_)Edit postfix conf file (in template)$($_WHITE_)"
#cp /etc/postfix/main.cf /tmp/template_postfix_main.cf
#lxc file push /tmp/template_postfix_main.cf z-${CT_NAME_START}-template/etc/postfix/main.cf
lxc file push $GIT_PATH/templates/all/etc/postfix/main.cf z-${CT_NAME_START}-template/etc/postfix/main.cf
lxc exec z-${CT_NAME_START}-template -- bash -c "
        sed -i                                      \
        -e 's/__FQDN__/$FQDN/'                  \
        -e 's/__IP_smtp_PRIV__/$IP_smtp_PRIV/'  \
        /etc/postfix/main.cf
"


# Copy /etc/crontab for Send crontab return to admin (CLOUD_TECH_ADMIN_EMAIL)
lxc file push /etc/crontab z-${CT_NAME_START}-template/etc/crontab

lxc stop z-${CT_NAME_START}-template --force

################################################################################

# Create all container from template
echo "$($_ORANGE_)Create and network configuration for all containers$($_WHITE_)"

CT_LIST="db www office ssh"

for CT in $CT_LIST ; do
    echo "$($_ORANGE_)Create ${CT_NAME_START}-${CT}...$($_WHITE_)"
    lxc copy z-${CT_NAME_START}-template ${CT_NAME_START}-${CT}
    lxc start ${CT_NAME_START}-${CT}
    IP_PUB="IP_PUB_${CT}"
    IP_PRIV="IP_PRIV_${CT}"
    #
    echo "$($_ORANGE_)${CT_NAME_START}-${CT}: DNS$($_WHITE_)"
    lxc exec ${CT_NAME_START}-${CT} -- bash -c "echo '$RESOLV_CONF' > /etc/resolv.conf"
    #
    echo "$($_ORANGE_)${CT_NAME_START}-${CT}: Network$($_WHITE_)"
    lxc exec ${CT_NAME_START}-${CT} -- bash -c "
        cat << EOF > /etc/network/interfaces
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet static
    address ${!IP_PUB}/$CIDR_PUBLIC
    gateway $DEFAULT_GW

auto ethPrivate
iface ethPrivate inet static
    address ${!IP_PRIV}/$CIDR_PRIVATE
EOF
    "
    #
    echo "$($_ORANGE_)${CT_NAME_START}-${CT}: Restart container$($_WHITE_)"
    lxc restart ${CT_NAME_START}-${CT} --force
done


################################################################################
# Delete template
lxc delete z-${CT_NAME_START}-template --force

################################################################################

# Create and attach deported directory
echo "$($_ORANGE_)Create and attach deported directory ($LXD_DEPORTED_DIR/…)$($_WHITE_)"

## Cloud
## - Nextcloud html directory
mkdir -p                                                \
    $LXD_DEPORTED_DIR/${CT_NAME_START}/www/var/www      \
    $LXD_DEPORTED_DIR/${CT_NAME_START}/www/etc/apache2
lxc config device add ${CT_NAME_START}-www shared-${CT_NAME_START}-www disk path=/srv/lxd source=$LXD_DEPORTED_DIR/${CT_NAME_START}/www/

# Nextcloud DATA directory
NEXTCLOUD_DATA_DIR="$LXD_DEPORTED_DIR/${CT_NAME_START}/data-nextcloud"
mkdir -p \
    $NEXTCLOUD_DATA_DIR
#chown 1000033:1000033 $NEXTCLOUD_DATA_DIR
lxc config device add ${CT_NAME_START}-www shared-${CT_NAME_START}-data-nextcloud disk path=/srv/data-nextcloud source=$NEXTCLOUD_DATA_DIR/

## Mariadb
## - Tempory directory for MySQL dump
mkdir -p \
    $LXD_DEPORTED_DIR/${CT_NAME_START}/db/var/lib/mysql
lxc config device add ${CT_NAME_START}-db shared-${CT_NAME_START}-db disk path=/srv/lxd source=$LXD_DEPORTED_DIR/${CT_NAME_START}/db/

# Set mapped UID and GID to LXD deported directory
echo "$($_ORANGE_)Set mapped UID and GID to LXD deported directory$($_WHITE_)"
echo "$($_ORANGE_)root:root for $LXD_DEPORTED_DIR/${CT_NAME_START}/*$($_WHITE_)"
chown -R 1000000:1000000 $LXD_DEPORTED_DIR/${CT_NAME_START}/*
echo "$($_ORANGE_)www-data:www-data for $NEXTCLOUD_DATA_DIR$($_WHITE_)"
chown -R 1000033:1000033 $NEXTCLOUD_DATA_DIR

################################################################################
#### CONTAINER CONFIGURATION
echo ""
echo "$($_GREEN_)CONTAINER CONFIGURATION$($_WHITE_)"
echo ""

############################################################

############################################################
#### MariaDB

$GIT_PATH/containers/11_configure_db.sh $PROJECT_NAME

############################################################
#### CLOUD
$GIT_PATH/containers/12_configure_www.sh $PROJECT_NAME

############################################################
#### SSH
$GIT_PATH/containers/13_configure_ssh.sh $PROJECT_NAME

################################################################################
#### OnlyOffice
$GIT_PATH/containers/14_configure_onlyoffice.sh $PROJECT_NAME

################################################################################
