#!/bin/bash

# BSD 3-Clause License
# 
# Copyright (c) 2018-2019, Alban Vidal <alban.vidal@zordhak.fr>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
# 
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
# 
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

################################################################################
##########                    Define color to output:                 ########## 
################################################################################
_WHITE_="tput sgr0"
_RED_="tput setaf 1"
_GREEN_="tput setaf 2"
_ORANGE_="tput setaf 3"
################################################################################

# Path of local git repository
# ../
GIT_PATH="$(realpath ${0%/*/*})"

# Load path of install_conf_lxd git directoy
source $GIT_PATH/config/00_VARS

################################################################################
#### Source local conf

source $GIT_PATH/config/NEXTCLOUD_GLOBAL_VARS

################################################################################
#### Source « install_conf_lxd » conf

# Load Vars
source $GIT_PATH_install_conf_lxd/config/00_VARS

# Load Network Vars
source $GIT_PATH_install_conf_lxd/config/01_NETWORK_VARS

# Load Resources Vars
source $GIT_PATH_install_conf_lxd/config/02_RESOURCES_VARS

# Load Other vars
# Used for :
# - $DEBIAN_RELEASE
source $GIT_PATH_install_conf_lxd/config/03_OTHER_VARS

################################################################################
# Load project vars
PROJECT_NAME=$1
source $GIT_PATH/config/project_${PROJECT_NAME}

# Define name of container
CT_NAME_START="nc-${PROJECT_NAME}"
CT_NAME_END="office"
CT_NAME="${CT_NAME_START}-${CT_NAME_END}"
################################################################################

#### OnlyOffice

################################################################################
echo
echo "$($_GREEN_)~~ BEGIN « $CT_NAME_END » container ~~$($_WHITE_)"

################################################################################

#echo "$($_ORANGE_)Create symlinks for /var/lib/mysql to /srv/lxd$($_WHITE_)"
#lxc exec ${CT_NAME} -- bash -c "
#    ln -s /srv/lxd/var/lib/mysql    /var/lib/
#"

################################################################################

## Install OnlyOffice
#echo "$($_ORANGE_)Install MariaDB serveur$($_WHITE_)"
#lxc exec ${CT_NAME} -- bash -c "
#    DEBIAN_FRONTEND=noninteractive apt-get -y install mariadb-server > /dev/null
#"

################################################################################

# Install key
echo "$($_ORANGE_)Install key$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "
    DEBIAN_FRONTEND=noninteractive apt-get -y install gnupg apt-transport-https > /dev/null
"

# Source list for nodejs 6.x
echo "$($_ORANGE_)Source list for nodejs 6.x$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 1655A0AB68576280 > /dev/null
    echo 'deb https://deb.nodesource.com/node_8.x stretch main' >  /etc/apt/sources.list.d/nodesource.list
"

# Source list onlyoffice
echo "$($_ORANGE_)Source listonlyoffice$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys CB2DE8E5 > /dev/null
    echo 'deb http://download.onlyoffice.com/repo/debian squeeze main' > /etc/apt/sources.list.d/onlyoffice.list
"

# Reload repository
echo "$($_ORANGE_)Reload repository$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "
    apt-get update > /dev/null
"

################################################################################

# Set OnlyOffice depandancies
echo "$($_ORANGE_)Install OnlyOffice depandancies$($_WHITE_)"
APP_LIST="                  \
libstdc++6                  \
nginx-extras                \
postgresql                  \
libxml2                     \
supervisor                  \
fonts-dejavu                \
fonts-liberation            \
fonts-crosextra-carlito     \
fonts-takao-gothic          \
fonts-opensymbol            \
sudo                        \
redis-server                \
rabbitmq-server             \
nodejs                      \
"

# Additionnal package for specific release
case $DEBIAN_RELEASE in
    "stretch" )
            APP_LIST="$APP_LIST \
                libcurl3 \
            "
    ;;
    "buster" )
            APP_LIST="$APP_LIST \
                libcurl4        \
            "
    ;;
    * )
        # Print error for unknown release
        echo "$($_RED_)Unknown Debian release$($_WHITE_)"
    ;;
esac

# Install OnlyOffice depandancies
lxc exec ${CT_NAME} -- bash -c "
    DEBIAN_FRONTEND=noninteractive apt-get -y install $APP_LIST > /dev/null
"

################################################################################

# Configure PostgresSQL
echo "$($_ORANGE_)Configure PostgresSQL$($_WHITE_)"
#cat << EOF > /tmp/conf_pgsql_${CT_NAME}
#    sudo -i -u postgres psql -c "CREATE DATABASE onlyoffice;"
#    # TODO: PASSWORD
#    sudo -i -u postgres psql -c "CREATE USER onlyoffice WITH password 'onlyoffice';"
#    sudo -i -u postgres psql -c "GRANT ALL privileges ON DATABASE onlyoffice TO onlyoffice;"
#EOF
#lxc file push --mode 500 /tmp/conf_pgsql_${CT_NAME} ${CT_NAME}/tmp/conf_pgsql
#rm -f /tmp/conf_pgsql_${CT_NAME}
#lxc exec ${CT_NAME} -- bash -c "
#    /tmp/conf_pgsql
#"
lxc exec ${CT_NAME} -- bash -c "
    sudo -i -u postgres psql -c \"CREATE DATABASE onlyoffice;\"
    # TODO: PASSWORD
    sudo -i -u postgres psql -c \"CREATE USER onlyoffice WITH password 'onlyoffice';\"
    sudo -i -u postgres psql -c \"GRANT ALL privileges ON DATABASE onlyoffice TO onlyoffice;\"
"

################################################################################

# Install OnlyOffice
echo "$($_ORANGE_)Install OnlyOffice$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "
    DEBIAN_FRONTEND=noninteractive apt-get -y install onlyoffice-documentserver > /dev/null
"

################################################################################

echo "$($_ORANGE_)Clean package cache (.deb files)$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "
    apt-get clean
"

################################################################################

echo "$($_ORANGE_)Reboot container to free memory$($_WHITE_)"
lxc restart ${CT_NAME} --force

echo "$($_ORANGE_)Set CPU and Memory limits$($_WHITE_)"
lxc profile add ${CT_NAME} $LXC_PROFILE_office_CPU
lxc profile add ${CT_NAME} $LXC_PROFILE_office_MEM

echo "$($_GREEN_)END OnlyOffice$($_WHITE_)"
echo ""

