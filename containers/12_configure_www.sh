#!/bin/bash

# BSD 3-Clause License
# 
# Copyright (c) 2018-2019, Alban Vidal <alban.vidal@zordhak.fr>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
# 
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
# 
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

################################################################################
##########                    Define color to output:                 ########## 
################################################################################
_WHITE_="tput sgr0"
_RED_="tput setaf 1"
_GREEN_="tput setaf 2"
_ORANGE_="tput setaf 3"
################################################################################
# Name of reverse proxy container
RVPRX_CONTAINER="rvprx"
################################################################################

# Path of local git repository
# ../
GIT_PATH="$(realpath ${0%/*/*})"

# Load path of install_conf_lxd git directoy
source $GIT_PATH/config/00_VARS

################################################################################
#### Source local conf

source $GIT_PATH/config/NEXTCLOUD_GLOBAL_VARS

################################################################################
#### Source « install_conf_lxd » conf

# Load Vars
source $GIT_PATH_install_conf_lxd/config/00_VARS

# Load Network Vars
source $GIT_PATH_install_conf_lxd/config/01_NETWORK_VARS

# Load Resources Vars
source $GIT_PATH_install_conf_lxd/config/02_RESOURCES_VARS

# Load Other vars 
# Used for :
# - $DEBIAN_RELEASE
source $GIT_PATH_install_conf_lxd/config/03_OTHER_VARS

################################################################################
# Load project vars
PROJECT_NAME=$1
source $GIT_PATH/config/project_${PROJECT_NAME}

# Define name of container
CT_NAME_START="nc-${PROJECT_NAME}"
CT_NAME_END="www"
CT_NAME="${CT_NAME_START}-${CT_NAME_END}"
################################################################################

#### CLOUD

echo "$($_GREEN_)BEGIN cloud$($_WHITE_)"

echo "$($_ORANGE_)Create symlinks for /etc/nginx and /etc/letsencrypt to /srv/lxd$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "
    ln -s /srv/lxd/etc/apache2      /etc/
    ln -s /srv/lxd/var/www/         /var/
"

echo "$($_ORANGE_)Generating certificates for: $FQDN_CLOUD and $FQDN_office$($_WHITE_)"
if $CREATE_SSL_CERTIFICATES ; then
    lxc exec $RVPRX_CONTAINER -- bash -c "certbot certonly -n --agree-tos --email $CLOUD_TECH_ADMIN_EMAIL --nginx -d $FQDN_CLOUD,$FQDN_office > /dev/null"
else
    echo "$($_GREEN_)CREATE_SSL_CERTIFICATES=false, don't create certificates, you need to setup it manually$($_WHITE_)"
fi

################################################################################

## Nginx vhost

echo "$($_ORANGE_)Nginx: Conf, Vhosts and tuning$($_WHITE_)"

# Test if /etc/nginx/RVPRX_common.conf file exist
lxc exec $RVPRX_CONTAINER -- bash -c "
    if [ ! -f /etc/nginx/RVPRX_common.conf ] ; then
        echo 'WARNING'
        echo 'No file /etc/nginx/RVPRX_common.conf are present'
        echo 'Needed for common nginx configuration'
        echo 'See https://framagit.org/zorval/lxd_deploy_apps/install_conf_lxd/blob/master/templates/rvprx/etc/nginx/RVPRX_common.conf'
    fi
"

################################################################################

echo "$($_ORANGE_)Create vhosts$($_WHITE_)"
#
echo "$($_ORANGE_)Create vhost: $FQDN_CLOUD$($_WHITE_)"
lxc file push $GIT_PATH/templates/rvprx/etc/nginx/sites-available/rvprx-cloud $RVPRX_CONTAINER/etc/nginx/sites-available/rvprx-${FQDN_CLOUD}.conf
lxc exec $RVPRX_CONTAINER -- bash -c "
    sed -i                                          \
        -e 's/__FQDN_CLOUD__/$FQDN_CLOUD/'          \
        -e 's/__IP_PRIV_www__/$IP_PRIV_www/'        \
        /etc/nginx/sites-available/rvprx-${FQDN_CLOUD}.conf
"
#
echo "$($_ORANGE_)Create vhost: $FQDN_office$($_WHITE_)"
lxc file push $GIT_PATH/templates/rvprx/etc/nginx/sites-available/rvprx-office $RVPRX_CONTAINER/etc/nginx/sites-available/rvprx-${FQDN_office}.conf
lxc exec $RVPRX_CONTAINER -- bash -c "
    sed -i                                                  \
        -e 's/__FQDN_CLOUD__/$FQDN_CLOUD/'                  \
        -e 's/__FQDN_office__/$FQDN_office/'                \
        -e 's/__IP_PRIV_office__/$IP_PRIV_office/'          \
        /etc/nginx/sites-available/rvprx-${FQDN_office}.conf
"

echo "$($_ORANGE_)Enable vhost$($_WHITE_)"
echo
lxc exec $RVPRX_CONTAINER -- bash -c "
    ln -sf /etc/nginx/sites-available/rvprx-${FQDN_CLOUD}.conf      /etc/nginx/sites-enabled/
    ln -sf /etc/nginx/sites-available/rvprx-${FQDN_office}.conf     /etc/nginx/sites-enabled/
    if nginx -t ; then
        echo
        echo -e '\033[32mNginx conf OK, reload\033[0m'
        systemctl reload nginx
    else
        echo
        echo -e '\033[31m!!! ERROR !!! Nginx fail, NO realod and disable vhosts\033[0m'
        echo 'rvprx-${FQDN_CLOUD}.conf'
        rm -f /etc/nginx/sites-enabled/rvprx-${FQDN_CLOUD}.conf
        echo 'rvprx-${FQDN_office}.conf'
        rm -f /etc/nginx/sites-enabled/rvprx-${FQDN_office}.conf
    fi
"

################################################################################

echo "$($_ORANGE_)Create « occ » alias command$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c 'echo "sudo -u www-data php /var/www/nextcloud/occ \$@" > /usr/local/bin/occ
                           chmod +x /usr/local/bin/occ
                           '

echo "$($_ORANGE_)Install packages for nextcloud...$($_WHITE_)"
case $DEBIAN_RELEASE in
    "stretch" )
        # Debian 9 => php7.0
        lxc exec ${CT_NAME} -- bash -c "
            DEBIAN_FRONTEND=noninteractive apt-get -y install \
                wget                \
                curl                \
                sudo                \
                apache2             \
                mariadb-client      \
                redis-server        \
                libapache2-mod-rpaf \
                php-redis           \
                php-imagick         \
                php7.0              \
                php7.0-fpm          \
                php7.0-xml          \
                php7.0-mysql        \
                php7.0-gd           \
                php7.0-zip          \
                php7.0-mbstring     \
                php7.0-curl         \
                php7.0-bz2          \
                php7.0-intl         \
                php7.0-mcrypt       \
                php7.0-gmp          \
            > /dev/null
        "
        ;;
    "buster" )
        # Debian 10 => php7.3
        lxc exec ${CT_NAME} -- bash -c "
            DEBIAN_FRONTEND=noninteractive apt-get -y install \
                wget                \
                curl                \
                sudo                \
                apache2             \
                mariadb-client      \
                redis-server        \
                libapache2-mod-rpaf \
                php                 \
                php-imagick         \
                php-redis           \
                php-fpm             \
                php-xml             \
                php-mysql           \
                php-gd              \
                php-zip             \
                php-mbstring        \
                php-curl            \
                php-bz2             \
                php-intl            \
                php-gmp             \
            > /dev/null
        "
        ;;
esac


echo "$($_ORANGE_)apache2 FIX ServerName$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "
    echo 'ServerName $FQDN_CLOUD' > /etc/apache2/conf-available/99_ServerName.conf
    a2enconf 99_ServerName > /dev/null
"

echo "$($_ORANGE_)Enable php7-fpm in apache2$($_WHITE_)"
case $DEBIAN_RELEASE in
    "stretch" )
        lxc exec ${CT_NAME} -- bash -c "
            a2enmod proxy_fcgi setenvif > /dev/null
            a2enconf php7.0-fpm > /dev/null
        "
        ;;
    "buster" )
        lxc exec ${CT_NAME} -- bash -c "
            a2enmod proxy_fcgi setenvif > /dev/null
            a2enconf php7.3-fpm > /dev/null
        "
        ;;
esac

echo "$($_ORANGE_)Enable apache2 mods$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "
    a2enmod rewrite > /dev/null
    a2enmod headers env dir mime > /dev/null
"

echo "$($_ORANGE_)Tuning opcache (php7) conf$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "
    sed -i                                                                              \
        -e 's/;opcache.enable=0/opcache.enable=1/'                                      \
        -e 's/;opcache.enable_cli=0/opcache.enable_cli=1/'                              \
        -e 's/;opcache.interned_strings_buffer=4/opcache.interned_strings_buffer=8/'    \
        -e 's/;opcache.max_accelerated_files=2000/opcache.max_accelerated_files=10000/' \
        -e 's/;opcache.memory_consumption=64/opcache.memory_consumption=128/'           \
        -e 's/;opcache.save_comments=1/opcache.save_comments=1/'                        \
        -e 's/;opcache.revalidate_freq=2/opcache.revalidate_freq=1/'                    \
        /etc/php/7.*/fpm/php.ini
"

echo "$($_ORANGE_)Set PHP memory limit to 512Mo$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "
    sed -i                                              \
        -e 's/^memory_limit = .*/memory_limit = 512M/'  \
        /etc/php/7.*/fpm/php.ini
"

echo "$($_ORANGE_)Restart FPM$($_WHITE_)"
case $DEBIAN_RELEASE in
    "stretch" )
        lxc exec ${CT_NAME} -- bash -c "systemctl restart php7.0-fpm.service"
        ;;
    "buster" )
        lxc exec ${CT_NAME} -- bash -c "systemctl restart php7.3-fpm.service"
        ;;
esac

echo "$($_ORANGE_)Restart apache2$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "systemctl restart apache2.service"

echo "$($_ORANGE_)Test Apache + PHP with FPM$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "cat << EOF > /var/www/html/phpinfo.php
<?php
phpinfo();
EOF"

lxc exec ${CT_NAME} -- bash -c 'if curl -s 127.0.0.1/phpinfo.php|grep -q php-fpm; then
    echo -e "\n\033[32m ** FPM OK **\033[00m\n"
else
    >&2 echo -e "\033[31m==================== ERROR  ====================\033[00m"
    >&2 echo -e "\033[31m==================== FPM HS ====================\033[00m"
    exit 1
fi'

lxc exec ${CT_NAME} -- bash -c "rm -f /var/www/html/phpinfo.php"

echo "$($_ORANGE_)apache2 listen only in Private IP$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "echo 'Listen $IP_PRIV_www:80' > /etc/apache2/ports.conf"

echo "$($_ORANGE_)Download and uncompress Nextcloud$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "wget -q $NEXTCLOUD_URL -O /tmp/nextcloud.tar.bz2"
lxc exec ${CT_NAME} -- bash -c "tar xaf /tmp/nextcloud.tar.bz2 -C /var/www/"
lxc exec ${CT_NAME} -- bash -c "rm -f /tmp/nextcloud.tar.bz2"

echo "$($_ORANGE_)Update directory privileges$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "
    chown -R www-data:www-data /var/www/nextcloud/
    mkdir -p /var/log/nextcloud
    chown -R www-data:www-data /var/log/nextcloud
    "

echo "$($_ORANGE_)Create Vhost apache for Nextcloud$($_WHITE_)"
lxc file push $GIT_PATH/templates/www/etc/apache2/sites-available/nextcloud.conf ${CT_NAME}/etc/apache2/sites-available/
lxc exec ${CT_NAME} -- bash -c "
    sed -i                                          \
        -e 's/__FQDN_CLOUD__/$FQDN_CLOUD/'          \
        -e 's/__MAIL_ADMIN__/$CLOUD_TECH_ADMIN_EMAIL/'    \
        /etc/apache2/sites-available/nextcloud.conf
"

echo "$($_ORANGE_)Disable Default Vhost and enable nextcloud$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "a2dissite 000-default > /dev/null
                           a2ensite nextcloud.conf > /dev/null"

echo "$($_ORANGE_)apache2 configtest$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "apache2ctl configtest"

echo "$($_ORANGE_)Reload apache2$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "systemctl reload apache2"

echo "$($_ORANGE_)Nextcloud installation$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "occ maintenance:install --database 'mysql' --database-host '$IP_PRIV_db' --database-name 'nextcloud'  --database-user '$DB_USERNAME' --database-pass '$DB_PASSWORD' --admin-user '$NEXTCLOUD_admin_user' --admin-pass '$NEXTCLOUD_admin_password' --data-dir='/srv/data-nextcloud'"

echo "$($_ORANGE_)Tune MAX upload file size$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "sed -i \
                             -e 's/upload_max_filesize=.*/upload_max_filesize=$MAX_UPLOAD_FILE_SIZE/' \
                             -e 's/post_max_size=.*/post_max_size=$MAX_UPLOAD_FILE_SIZE/' \
                             /var/www/nextcloud/.user.ini
                           "

echo "$($_ORANGE_)Tune Nextcloud conf$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "occ config:system:set trusted_domains 0    --value='$FQDN_CLOUD'
                           occ config:system:set overwrite.cli.url    --value='$FQDN_CLOUD'
                           occ config:system:set htaccess.RewriteBase --value='/'
                           # Language and time zone settings
                           occ config:system:set default_language     --value='$LANGUAGE'
                           occ config:system:set force_language       --value='$LANGUAGE'
                           occ config:system:set default_locale       --value='$LOCALE'
                           occ config:system:set force_locale         --value='$LOCALE'
                           occ config:system:set logtimezone          --value='$TIME_ZONE'
                           # Redis
                           occ config:system:set memcache.local       --value='\\OC\\Memcache\\Redis'
                           occ config:system:set memcache.locking     --value='\\OC\\Memcache\\Redis'
                           occ config:system:set redis host           --value='localhost'
                           occ config:system:set redis port           --value='6379'
                           # Log
                           occ config:system:set loglevel             --value='2'
                           occ config:system:set logfile              --value='/var/log/nextcloud/nextcloud.log'
                           # Example for 100MB :
                           # 100MB ( 100 * 1024 * 1024 ) = 104857600 byte
                           occ config:system:set log_rotate_size      --value=$(( $NEXTCLOUD_LOG_ROTATE_SIZE * 1024 * 1024 ))
                           # Use cron to run background jobs
                           occ background:cron
                           "

echo "$($_ORANGE_)Update .htaccess$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "occ maintenance:update:htaccess > /dev/null"

echo "$($_ORANGE_)Install applications in Nextcloud$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "
    occ app:install calendar
    occ app:enable  calendar
    occ app:enable  admin_audit
    occ app:install contacts
    occ app:enable  contacts
    occ app:install announcementcenter
    occ app:enable  announcementcenter
    # OnlyOffice
    occ app:install onlyoffice
    occ app:enable  onlyoffice
    occ app:install quota_warning
    occ app:enable  quota_warning
    occ app:install files_rightclick
    occ app:enable  files_rightclick
    occ app:enable  files_pdfviewer
    # Talk
    occ app:install spreed
    occ app:enable  spreed
    # Registration - WARNING: need to sed admin_approval_required to true !
    occ app:install registration
    occ app:enable registration
    # allows you to customize your share tokens
    occ app:install sharerenamer
    occ app:enable sharerenamer
    # Notes
    occ app:install notes
    occ app:enable  notes
    # Tasks
    occ app:install tasks
    occ app:enable  tasks
    # Group Folders
    occ app:enable groupfolders
    # Text - Collaborative document editing
    occ app:enable text
"

echo "$($_ORANGE_)Enable admin_approval_required for registration app$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "
    occ config:app:set registration admin_approval_required --value='yes'
"

echo "$($_ORANGE_)Enable quota warning notification$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "occ config:app:set quota_warning info_email    --value='yes'
                           occ config:app:set quota_warning warning_email --value='yes'
                           occ config:app:set quota_warning alert_email   --value='yes'
                           "

echo "$($_ORANGE_)Configure SMTP in Nextcloud$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "occ config:system:set mail_smtpmode --value='smtp'
                           occ config:system:set mail_smtpauthtype --value='LOGIN'
                           occ config:system:set mail_from_address --value='cloud'
                           occ config:system:set mail_domain --value='$FQDN'
                           occ config:system:set mail_smtphost --value='$IP_smtp_PRIV'
                           occ config:system:set mail_smtpport --value='25'
                           "

echo "$($_ORANGE_)Set OnlyOffice url$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "
    occ config:app:set onlyoffice DocumentServerUrl --value='https://$FQDN_office'
"

echo "$($_ORANGE_)Set email in Nextcloud admin account$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "occ user:setting $NEXTCLOUD_admin_user settings email '$NEXTCLOUD_admin_email'"

echo "$($_ORANGE_)Set transparency client ip for Nextcloud and Apache$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "
    occ config:system:set trusted_proxies 0 --value='$IP_rvprx_PRIV'
    occ config:system:set forwarded_for_headers 0 --value='HTTP_X_FORWARDED_FOR'
    sed -i 's/\\(.*RPAFproxy_ips\\).*/\\1 $IP_rvprx_PRIV/' /etc/apache2/mods-available/rpaf.conf
"

################################################################################

echo "$($_ORANGE_)Defining background jobs via systemd timer$($_WHITE_)"
lxc file push $GIT_PATH/templates/www/etc/systemd/system/nextcloudcron.service ${CT_NAME}/etc/systemd/system/
lxc file push  $GIT_PATH/templates/www/etc/systemd/system/nextcloudcron.timer ${CT_NAME}/etc/systemd/system/
lxc exec ${CT_NAME} -- bash -c "
    systemctl start nextcloudcron.timer
    systemctl enable nextcloudcron.timer
"


################################################################################
# TODO
# Espaces HS dans NC_theming...
echo "$($_ORANGE_)Set Bottum name slogan and url$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "
    occ config:app:set theming name   --value='$NC_theming_name'
#    occ config:app:set theming slogan --value='$NC_theming_slogan'
    occ config:app:set theming url    --value='$NC_theming_url'
"

################################################################################

echo "$($_ORANGE_)NC Database - convert filecache bigint$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "
    sudo -u www-data php occ db:convert-filecache-bigint
"

################################################################################

#echo "$($_ORANGE_)Add Let's Encrypt chain.pem (CA Root Certificates) in Nextcloud$($_WHITE_)"
#chain_pem="$(lxc exec rvprx -- cat /etc/letsencrypt/live/$FQDN/chain.pem)"
#lxc exec cloud -- bash -c "cat << 'EOF' >> /var/www/nextcloud/resources/config/ca-bundle.crt
#
#Let's Encrypt
#=============
#$chain_pem
#EOF
#"

echo "$($_ORANGE_)Clean package cache (.deb files)$($_WHITE_)"
lxc exec ${CT_NAME} -- bash -c "apt-get clean"

echo "$($_ORANGE_)Reboot container to free memory$($_WHITE_)"
lxc restart ${CT_NAME} --force

echo "$($_ORANGE_)Set CPU and Memory limits$($_WHITE_)"
lxc profile add ${CT_NAME} $LXC_PROFILE_www_CPU
lxc profile add ${CT_NAME} $LXC_PROFILE_www_MEM

echo "$($_GREEN_)END cloud$($_WHITE_)"
echo ""

